package com.example.mylauncher.util

import com.example.mylauncher.models.CategorizedAppModel
import com.example.mylauncher.models.InstalledAppModel
import com.example.mylauncher.room.models.FinalAppModel

fun InstalledAppModel.toCategorizedAppModel(appCategory: AppCategory) = CategorizedAppModel(
    appPackageName = this.appPackageName,
    appName = this.appName,
//    appIcon = this.appIcon,
    appCategory = appCategory
)

fun InstalledAppModel.toDefaultFinalAppModel() = FinalAppModel(
    appPackageName = this.appPackageName,
    appName = this.appName,
    appIcon = UtilityFunctions.drawableToBitmap(this.appIcon),
    appCategory = AppCategory.DEFAULT,
    isModified = false,
    isSecured = false,
    customAppName = null,
    customAppIcon = null
)