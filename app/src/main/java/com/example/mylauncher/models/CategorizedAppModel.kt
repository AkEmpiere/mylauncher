package com.example.mylauncher.models

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.example.mylauncher.util.AppCategory

data class CategorizedAppModel(
    val appPackageName: String,
    val appName: String,
//    val appIcon: Drawable,
    val appCategory: AppCategory
)
