package com.example.mylauncher.models

import android.graphics.drawable.Drawable

data class InstalledAppModel(
    val appPackageName: String,
    val appName: String,
    val appIcon: Drawable
)
