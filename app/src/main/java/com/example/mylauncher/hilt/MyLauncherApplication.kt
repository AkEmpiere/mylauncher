package com.example.mylauncher.hilt

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyLauncherApplication : Application() {
}