package com.example.mylauncher.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.mylauncher.ui.fragment.show_apps_list_fragment.ShowAppListFragment

private const val TAG = "AppViewPagerAdapter"

class AppViewPagerAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
    private var fragments: MutableList<ShowAppListFragment>
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }

    fun add(index: Int, fragment: ShowAppListFragment) {
        fragments.add(index, fragment)
        notifyItemChanged(index)
    }

    fun remove(index: Int) {
        if (fragments.isNotEmpty() && fragments.size > index) {
            fragments.removeAt(index)
            notifyItemChanged(index)
        }
    }

    fun addAll(fragment: MutableList<ShowAppListFragment>) {
        fragments = fragment
        notifyDataSetChanged()
    }

    fun refreshFragment(index: Int, fragment: ShowAppListFragment) {
        fragments[index] = fragment
        notifyItemChanged(index)
    }

    override fun getItemId(position: Int): Long {
        return fragments[position].hashCode().toLong()
    }

    override fun containsItem(itemId: Long): Boolean {
        return fragments.find { it.hashCode().toLong() == itemId } != null
    }
}