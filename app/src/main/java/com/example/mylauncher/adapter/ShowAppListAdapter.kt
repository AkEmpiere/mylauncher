package com.example.mylauncher.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mylauncher.databinding.ItemAppBinding
import com.example.mylauncher.room.models.FinalAppModel
import javax.inject.Inject

class ShowAppListAdapter @Inject constructor() :
    ListAdapter<FinalAppModel, ShowAppListAdapter.ShowAppListViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowAppListViewHolder {
        val binding =
            ItemAppBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ShowAppListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShowAppListViewHolder, position: Int) {
        val currentItem = getItem(position)

        holder.bind(currentItem)
    }

    inner class ShowAppListViewHolder(private val binding: ItemAppBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(finalAppModel: FinalAppModel) {
            binding.apply {
                if (finalAppModel.isModified)
                    appIconImg.setImageBitmap(finalAppModel.customAppIcon)
                else
                    appIconImg.setImageBitmap(finalAppModel.appIcon)

                appNameTxt.text = finalAppModel.appName

                appParentLayout.setOnClickListener {
                    onAppClickListener?.let { it(finalAppModel) }
                }
            }
        }
    }

    private var onAppClickListener: ((FinalAppModel) -> Unit)? = null
    fun setAppClickListener(appInfo: (FinalAppModel) -> Unit) {
        onAppClickListener = appInfo
    }

    class DiffCallback : DiffUtil.ItemCallback<FinalAppModel>() {
        override fun areItemsTheSame(oldItem: FinalAppModel, newItem: FinalAppModel) =
            oldItem.appPackageName == newItem.appPackageName

        override fun areContentsTheSame(oldItem: FinalAppModel, newItem: FinalAppModel) =
            oldItem == newItem
    }
}