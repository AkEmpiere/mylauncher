package com.example.mylauncher.adapter

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager.widget.PagerAdapter.POSITION_NONE
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.mylauncher.room.models.FinalAppModel
import com.example.mylauncher.ui.fragment.show_apps_list_fragment.ShowAppListFragment
import com.example.mylauncher.util.AppCategory

private const val TAG = "BackUpAppViewPagerAdapter"

class BackUpAppViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    private var finalAppsValues: ArrayList<List<FinalAppModel>> = arrayListOf<List<FinalAppModel>>()

    override fun getItemCount(): Int {
        return finalAppsValues.size
    }

    override fun createFragment(position: Int): Fragment {
        return ShowAppListFragment.newInstance(finalAppsValues[position] as ArrayList<FinalAppModel>)
    }

    override fun getItemId(position: Int): Long {
        return POSITION_NONE.toLong()
    }

    fun setItems(
        newItems: ArrayList<List<FinalAppModel>>,
        wholeItems: List<FinalAppModel>
    ) {
        Log.d(TAG, "setItems: called")
        finalAppsValues = arrayListOf()
        if (newItems.size > 1)
            finalAppsValues.add(0, wholeItems)
        finalAppsValues.addAll(newItems)
        notifyDataSetChanged()
    }
}