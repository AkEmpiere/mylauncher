package com.example.mylauncher.ui.fragment.home_fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.mylauncher.R
import com.example.mylauncher.databinding.FragmentHomeBinding
import com.example.mylauncher.room.models.FinalAppModel
import com.example.mylauncher.ui.viewmodels.shared_viewmodel.AppsViewModel
import com.example.mylauncher.util.AppCategory

private const val TAG = "HomeFragment"

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    //ViewModels
    private val appsViewModel by activityViewModels<AppsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        observeLiveData()
        binding.appsDrawerBtn.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_appsFragment)
        }
    }

    private fun observeLiveData() {
        appsViewModel.installedAppModels.observe(viewLifecycleOwner) { installedAppModelList ->
            Log.i(TAG, "observe InstalledAppModels LiveData: ${installedAppModelList.size}")

            if (!installedAppModelList.isNullOrEmpty()) {
                appsViewModel.saveDefaultFinalAppModelIntoDb(installedAppModelList)
            }
        }

        appsViewModel.categorizedAppModels.observe(viewLifecycleOwner) { categorizedAppModelList ->
            Log.i(TAG, "observe CategorizedAppModels LiveData: ${categorizedAppModelList.size}")

            if (!categorizedAppModelList.isNullOrEmpty()) {
                appsViewModel.updateAppCategoryIntoDb(categorizedAppModelList)
            }
        }

        appsViewModel.finalAppModels.observe(viewLifecycleOwner) { finalAppModelList ->

            if (!finalAppModelList.isNullOrEmpty()) {
                Log.i(
                    TAG,
                    "observe FinalAppModels LiveData: ${finalAppModelList.size}}"
                )

                val map: Map<AppCategory, List<FinalAppModel>> = finalAppModelList.groupBy(keySelector = { it.appCategory })

                Log.i(TAG, "---------------------------------------------------------------")
                map.forEach { (appCategory, list) ->
                    Log.i(TAG, "$appCategory/${list.size} -> $list")
                }
                Log.i(TAG, "---------------------------------------------------------------")
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}