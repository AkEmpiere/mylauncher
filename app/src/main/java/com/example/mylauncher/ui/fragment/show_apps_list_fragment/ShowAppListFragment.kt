package com.example.mylauncher.ui.fragment.show_apps_list_fragment

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mylauncher.R
import com.example.mylauncher.adapter.ShowAppListAdapter
import com.example.mylauncher.databinding.FragmentShowAppListBinding
import com.example.mylauncher.room.models.FinalAppModel
import com.example.mylauncher.util.FINAL_APP_PARAM
import com.example.mylauncher.util.ItemOffsetDecoration
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


const val TAG = "ShowAppListFragment"

@AndroidEntryPoint
class ShowAppListFragment : Fragment() {

    private var _binding: FragmentShowAppListBinding? = null
    private val binding get() = _binding!!

    private var finalAppModelList: List<FinalAppModel>? = null

    @Inject
    lateinit var showAppListAdapter: ShowAppListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            finalAppModelList = it.getParcelableArrayList<FinalAppModel>(FINAL_APP_PARAM)
        }
        arguments?.clear()
        Log.i(TAG, "onCreate: FinalAppModelList found - ${finalAppModelList.toString()}")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentShowAppListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerView()
        setUpRecyclerViewClickListener()
    }

    private fun setUpRecyclerViewClickListener() {
        showAppListAdapter.setAppClickListener { finalAppModel ->
            try {
                requireContext().startActivity(
                    requireContext().packageManager.getLaunchIntentForPackage(
                        finalAppModel.appPackageName
                    )
                )
            } catch (e: Exception) {
                requireContext().startActivity(
                    requireContext().packageManager.getLaunchIntentForPackage("com.example.mylauncher")
                )
//                showAppsViewModel.getInstalledAppData()
            }
        }
    }

    private fun setUpRecyclerView() {
        binding.showAppsRecycleView.layoutManager = GridLayoutManager(requireContext(), 4)
        val itemDecoration = ItemOffsetDecoration(requireContext(), R.dimen.item_offset)
        binding.showAppsRecycleView.addItemDecoration(itemDecoration)
        binding.showAppsRecycleView.adapter = showAppListAdapter
        finalAppModelList?.let {
            showAppListAdapter.submitList(it)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(arrayListOfFinalAppModel: ArrayList<FinalAppModel>) =
            ShowAppListFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(FINAL_APP_PARAM, arrayListOfFinalAppModel)
                }
            }
//        @JvmStatic
//        fun newInstance() =
//            ShowAppListFragment()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
//        outState.clear()
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
