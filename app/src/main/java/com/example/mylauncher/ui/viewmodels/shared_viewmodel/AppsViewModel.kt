package com.example.mylauncher.ui.viewmodels.shared_viewmodel

import android.app.Application
import android.content.Intent
import android.content.pm.ResolveInfo
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mylauncher.models.CategorizedAppModel
import com.example.mylauncher.models.InstalledAppModel
import com.example.mylauncher.room.dao.AppsDao
import com.example.mylauncher.room.models.FinalAppModel
import com.example.mylauncher.util.AppCategoryService
import com.example.mylauncher.util.toCategorizedAppModel
import com.example.mylauncher.util.toDefaultFinalAppModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val TAG = "AppsViewModel"

@HiltViewModel
class AppsViewModel @Inject constructor(
    private val applicationContext: Application,
    private val appCategoryService: AppCategoryService,
    private val appsDao: AppsDao
) : ViewModel() {

    // For getting installed app list
    private val _installedAppModels = MutableLiveData<List<InstalledAppModel>>()
    val installedAppModels: LiveData<List<InstalledAppModel>> = _installedAppModels

    // For getting Categorized installed app list
    private val _categorizedAppModels = MutableLiveData<List<CategorizedAppModel>>()
    val categorizedAppModels: LiveData<List<CategorizedAppModel>> = _categorizedAppModels

    // For getting finalAppModel data from room db
    val finalAppModels: LiveData<List<FinalAppModel>> = appsDao.getAllFinalApp()

    init {
        clearFinalAppDb()
        getInstalledApps()
    }

    private fun getInstalledApps() {
        Log.i(TAG, "getInstalledApps: called")

        viewModelScope.launch {
            val packageManager = applicationContext.packageManager
            val resolvedAppList: List<ResolveInfo> = packageManager
                .queryIntentActivities(
                    Intent(Intent.ACTION_MAIN, null)
                        .addCategory(Intent.CATEGORY_LAUNCHER), 0
                )

            val tempInstalledAppList = mutableListOf<InstalledAppModel>()
            resolvedAppList.forEach { resolveInfo ->
                if (resolveInfo.activityInfo.packageName != applicationContext.packageName) {
                    val app = InstalledAppModel(
                        appPackageName = resolveInfo.activityInfo.packageName,
                        appName = resolveInfo.loadLabel(packageManager).toString(),
                        appIcon = resolveInfo.activityInfo.loadIcon(packageManager)
                    )
                    tempInstalledAppList.add(app)
                }
            }
            _installedAppModels.postValue(emptyList())
            _installedAppModels.postValue(tempInstalledAppList)
        }
    }

    private fun getCategorizedApps(installedAppList: List<InstalledAppModel>) {
        Log.i(TAG, "getCategorizedApps: called")
        viewModelScope.launch {
            val tempCategorizedAppList = mutableListOf<CategorizedAppModel>()
            installedAppList.forEach { installedAppModel ->
                val appCategory = appCategoryService.fetchCategory(installedAppModel.appPackageName)
                val categorizedAppModel = installedAppModel.toCategorizedAppModel(appCategory)
                tempCategorizedAppList.add(categorizedAppModel)
            }
            _categorizedAppModels.postValue(emptyList())
            _categorizedAppModels.postValue(tempCategorizedAppList)
            Log.i(TAG, "getCategorizedApps: over")
        }
    }

    fun saveDefaultFinalAppModelIntoDb(installedAppModelList: List<InstalledAppModel>) {
        viewModelScope.launch(IO) {
            Log.i(TAG, "saveDefaultFinalAppModelIntoDb: called")
            appsDao.clearFinalAppDb()
            installedAppModelList.forEach {
                appsDao.insertFinalApp(it.toDefaultFinalAppModel())
            }
            Log.i(TAG, "saveDefaultFinalAppModelIntoDb: over")
            getCategorizedApps(installedAppModelList)
        }
    }

    fun updateAppCategoryIntoDb(categorizedAppModelList: List<CategorizedAppModel>) {
        viewModelScope.launch(IO) {
            Log.i(TAG, "updateAppCategoryIntoDb: called")
            categorizedAppModelList.forEach {
                appsDao.updateAppCategoryField(
                    category = it.appCategory,
                    packageName = it.appPackageName
                )
            }
            Log.i(TAG, "updateAppCategoryIntoDb: over")
        }
    }

    private fun clearFinalAppDb() {
        viewModelScope.launch {
            appsDao.clearFinalAppDb()
        }
    }
}