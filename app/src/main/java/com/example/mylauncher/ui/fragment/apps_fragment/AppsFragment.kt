package com.example.mylauncher.ui.fragment.apps_fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.mylauncher.adapter.AppViewPagerAdapter
import com.example.mylauncher.databinding.FragmentAppsBinding
import com.example.mylauncher.room.models.FinalAppModel
import com.example.mylauncher.ui.fragment.show_apps_list_fragment.ShowAppListFragment
import com.example.mylauncher.ui.viewmodels.shared_viewmodel.AppsViewModel
import com.example.mylauncher.util.AppCategory
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "AppsFragment"

@AndroidEntryPoint
class AppsFragment : Fragment() {

    private var _binding: FragmentAppsBinding? = null
    private val binding get() = _binding!!

    //ViewModels
    private val appsViewModel by activityViewModels<AppsViewModel>()

    //Adapters
    private lateinit var appViewPagerAdapter: AppViewPagerAdapter

    //Global variables for ViewPager:
    private val fragmentList = mutableListOf<ShowAppListFragment>()
    var listOfFinalAppsMapKey: ArrayList<AppCategory>? = null
    var listOfFinalAppsMapValues: ArrayList<List<FinalAppModel>>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAppsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated: called")

        setUpViewPagerAndTabLayout()
        observeLiveData()

    }

    private fun setUpViewPagerAndTabLayout() {
        appViewPagerAdapter = AppViewPagerAdapter(childFragmentManager, lifecycle, fragmentList)
        binding.viewPager.adapter = appViewPagerAdapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            Log.d(TAG, "setUpViewPagerAndTabLayout: tab and position is : $position")
            tab.text =
//                AppCategory.DEFAULT.name
                if (position == 0) {
                    AppCategory.DEFAULT.name
                } else {
                    listOfFinalAppsMapKey?.let {
                        it[position - 1].name
                    } ?: AppCategory.DEFAULT.name
                }
        }.attach()
    }

    private fun observeLiveData() {
        appsViewModel.installedAppModels.observe(viewLifecycleOwner) { installedAppModelList ->
            Log.i(TAG, "observe InstalledAppModels LiveData: ${installedAppModelList.size}")

            if (!installedAppModelList.isNullOrEmpty()) {
                appsViewModel.saveDefaultFinalAppModelIntoDb(installedAppModelList)
            }
        }

        appsViewModel.categorizedAppModels.observe(viewLifecycleOwner) { categorizedAppModelList ->
            Log.i(TAG, "observe CategorizedAppModels LiveData: ${categorizedAppModelList.size}")

            if (!categorizedAppModelList.isNullOrEmpty()) {
                appsViewModel.updateAppCategoryIntoDb(categorizedAppModelList)
            }
        }

        appsViewModel.finalAppModels.observe(viewLifecycleOwner) { finalAppModelList ->
            if (!finalAppModelList.isNullOrEmpty()) {
                Log.i(TAG, "observe FinalAppModels : ${finalAppModelList.size}")

                val finalAppsMap: Map<AppCategory, List<FinalAppModel>> =
                    finalAppModelList.groupBy(keySelector = { it.appCategory })

                Log.i(TAG, "--------------------------------")
                finalAppsMap.forEach { (appCategory, list) ->
                    Log.i(TAG, "$appCategory/${list.size} -> $list")
                }
                Log.i(TAG, "--------------------------------")

                listOfFinalAppsMapKey = ArrayList(finalAppsMap.keys)
                listOfFinalAppsMapValues = ArrayList(finalAppsMap.values)
                listOfFinalAppsMapValues?.let {
                    if (it.size > 1) {
                        listOfFinalAppsMapValues!!.add(0, finalAppModelList)
                    }
                }
                listOfFinalAppsMapValues?.forEachIndexed { index, list ->
                    appViewPagerAdapter.remove(index)
                    appViewPagerAdapter.add(
                        index,
                        ShowAppListFragment.newInstance(list as ArrayList<FinalAppModel>)
                    )
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.clear()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}