package com.example.mylauncher.ui.fragment.set_custom_details_fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mylauncher.R
import com.example.mylauncher.databinding.FragmentSetCustomDetailsBinding

class SetCustomDetailsFragment : Fragment() {

    private var _binding: FragmentSetCustomDetailsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSetCustomDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}