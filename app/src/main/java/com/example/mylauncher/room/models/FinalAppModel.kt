package com.example.mylauncher.room.models

import android.graphics.Bitmap
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.mylauncher.util.AppCategory
import com.example.mylauncher.util.TABLE_NAME
import kotlinx.parcelize.Parcelize


@Parcelize
@Entity(tableName = TABLE_NAME)
data class FinalAppModel(
    @PrimaryKey(autoGenerate = false)
    val appPackageName: String,
    val appName: String,
    val appIcon: Bitmap,
    val appCategory: AppCategory,
    val isModified: Boolean = false,
    val isSecured: Boolean = false,
    val customAppName: String? = null,
    val customAppIcon: Bitmap? = null
) : Parcelable
