package com.example.mylauncher.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mylauncher.room.models.FinalAppModel
import com.example.mylauncher.util.AppCategory

@Dao
interface AppsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFinalApp(finalAppModel: FinalAppModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllFinalApp(vararg finalAppModels: FinalAppModel)

    @Update
    suspend fun update(finalAppModel: FinalAppModel)

    @Delete
    suspend fun delete(finalAppModel: FinalAppModel)

    @Query("SELECT * FROM final_app_table")
    fun getAllFinalApp(): LiveData<List<FinalAppModel>>

    @Query("DELETE FROM final_app_table")
    suspend fun clearFinalAppDb()

    @Query("UPDATE final_app_table SET appCategory= :category WHERE appPackageName LIKE :packageName")
    suspend fun updateAppCategoryField(category: AppCategory, packageName: String)
}