package com.example.mylauncher.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.mylauncher.room.converters.Converters
import com.example.mylauncher.room.dao.AppsDao
import com.example.mylauncher.room.models.FinalAppModel

@Database(
    entities = [FinalAppModel::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppsDatabase : RoomDatabase() {

    abstract fun getAppsDao(): AppsDao

}